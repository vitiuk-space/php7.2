FROM 313u/nginx:1.17.5
MAINTAINER Andrew Vityuk <andruwa13@me.com>

ENV PHP_MAJOR 7.2

ENV PHP_VERSION 7.2.28*

ENV DEFAULT_CHOWN_APP_DIR true
ENV DEFAULT_VIRTUAL_HOST example.com
ENV DEFAULT_APP_HOSTNAME example.com
ENV DEFAULT_APP_USER www-data
ENV DEFAULT_APP_GROUP www-data
ENV DEFAULT_APP_UID 33
ENV DEFAULT_APP_GID 33
ENV DEFAULT_UPLOAD_MAX_SIZE 128M
ENV DEFAULT_NGINX_MAX_WORKER_PROCESSES 8
ENV DEFAULT_NGINX_KEEPALIVE_TIMEOUT 30
ENV DEFAULT_PHP_MEMORY_LIMIT 128M
ENV DEFAULT_PHP_MAX_FILE_UPLOAD 200
ENV DEFAULT_PHP_MAX_EXECUTION_TIME 800
ENV DEFAULT_PHP_MAX_INPUT_VARS 800

RUN add-apt-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get -y --no-install-recommends --allow-unauthenticated install \
    ssmtp \
    git \
    sudo \
    php${PHP_MAJOR}=${PHP_VERSION} \
    php${PHP_MAJOR}-fpm=${PHP_VERSION} \
    php${PHP_MAJOR}-mysql \
    php${PHP_MAJOR}-cli \
    php${PHP_MAJOR}-curl \
    php${PHP_MAJOR}-gd \
    php${PHP_MAJOR}-intl \
    php${PHP_MAJOR}-imagick \
    php${PHP_MAJOR}-imap \
    php${PHP_MAJOR}-sqlite3 \
    php${PHP_MAJOR}-zip \
    php${PHP_MAJOR}-mbstring \
    php${PHP_MAJOR}-redis \
    php${PHP_MAJOR}-memcached \
    php${PHP_MAJOR}-xml \
    php${PHP_MAJOR}-bz2 \
    php${PHP_MAJOR}-soap \
    php${PHP_MAJOR}-ldap \
    php${PHP_MAJOR}-bcmath \
    php${PHP_MAJOR}-recode && \
    apt-get clean && \
    apt-get autoremove && \
    rm -Rf /tmp/* /var/tmp/* /var/lib/apt/lists/*

#WP-CLI
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
&& php wp-cli.phar --info \
&& chmod +x wp-cli.phar \
&& sudo mv wp-cli.phar /usr/local/bin/wp

RUN mkdir /root/bin/ && \
	echo "export PATH=/root/bin:$PATH" > /root/.bashrc

EXPOSE 443
EXPOSE 80

COPY . /app
COPY ssmtp.conf /etc/ssmtp/ssmtp.conf

RUN chmod 750 /app/bin/*

RUN /app/bin/init_php.sh

ENTRYPOINT ["/app/bin/boot.sh"]

CMD ["/sbin/my_init"]
